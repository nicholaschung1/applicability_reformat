from report_generator import get_redshift
import pandas as pd
import numpy as np
import regex as re
import redshift_connector

redshift_ca_summary = ("select * from rivian_datalake_rpt.enovia_ca_summary_rpt")

df_ca_summary = get_redshift(redshift_ca_summary)
df_ca_summary = df_ca_summary[['name', 'applicability']]

table_list = [['ca_number', 'program', 'build_event', 'effectivity_in', 'effectivity_out', 'manufacturing_build_event']]
table_list_size = len(table_list)

# split row by program, build_event, and date_effectivity
def clean_up(txt, program, build_event, date_effectivity):
    x = re.findall('[^[]*\[([^]]*)\]', txt)
    # date puller
    for item in x:
        y = item.split(' - ')
        for date in y:
            if date == '':
                date = 'INF'
            # set INF variable to date format
            if date == 'INF':
                date = '9999-11-11 00:00:00'
            date_effectivity.append(date)
    for item in x:
        txt = txt.replace('[' + item + ']', '')

    build_event_split = []

    if ' OR ' in txt:
        build_event_split = txt.split(' OR ')
    else:
        build_event_split = txt.split('AND')

    for item in build_event_split:
        program_split = item.split('<')    
        for i in program_split:
            if i == '':
                continue
            split2 = i.split(':')
            for j in split2:
                k = j.strip()
                if k == '':
                    continue
                else:
                    if '-' in k:
                        build_event.append(k)
                        print(build_event)
                    else:
                        program.append(k)

def new_row():
    table_list = [['ca_number', 'program', 'build_event', 'effectivity_in', 'effectivity_out', 'manufacturing_build_event', 'original_string']]
    table_list_size = len(table_list)
    
    new_row = []
    for i in range(len(table_list[0])):
        new_row.append(np.nan)
    return new_row

def add_new_row(row, ca_number, program, build_event, date_effectivity, original_string, stop = ''):
    m_build_event = []
    # use program name as key
    for i, curr_program in enumerate(program):
        row.append(new_row())
        row[i][0] = ca_number
        row[i][1] = curr_program
        row[0][6] = original_string
        # if program not given
        for item in build_event:
            if curr_program == 'no program given':
                if 'M' not in item:
                    row[i][2] = item
                if 'M' in item:
                    row[i][5] = item
                break
            x = item.split('-')

            # parse engineering build_events 
            if 'M' not in item:
                order_0 = ['VP1', 'VP2', 'VP3', 'TT', 'PP', 'SOP']
                i_order_0 = [5, 4, 3, 2, 1, 0]
                order_1 = ['VP', 'TT', 'PP', 'SOP']
                i_order_1 = [3, 2, 1, 0]
                order_RPV = ['VB', 'VP', 'TT', 'PP', 'SOP']
                i_order_RPV = [4, 3, 2, 1, 0]
                order_2 = ['EVT1', 'EVT2', 'DVT1', 'DVT2', 'PVT']
                i_order_2 = [4, 3, 2, 1, 0]
                order_3 = ['DV1', 'DV2']
                i_order_3 = [1, 0]
                order_4 = ['SALEABLE', 'SOP']
                i_order_4 = [1, 0]

                true_order = []
                true_i_order = []
                
                for aa in order_1:
                    if aa in item:
                        true_order = order_1
                        true_i_order = i_order_1
                for bb in order_2:
                    if bb in item:
                        true_order = order_2
                        true_i_order = i_order_2
                for rr in order_RPV:
                    if rr in item:
                        true_order = order_RPV
                        true_i_order = i_order_RPV
                for cc in order_3:
                    if cc in item:
                        true_order = order_3
                        true_i_order = i_order_3

                for oo in order_0[:3]:
                    if oo in item:
                        true_order = order_0
                        true_i_order = i_order_0
                if item == 'SALEABLE':
                    true_order = order_4
                    true_i_order = i_order_4

                if 'BATTERY' in item:
                    true_order = []

                row[i][2] = x[1]

                # example: BATTERY_SOP
                if '_' in x[1]:
                    ii = x[1].split('_')
                    x[1] = ii[1]

                if true_order:
                    find_index = true_order.index(x[1])
                    while true_order:
                        curr = true_order.pop(0)
                        if x[1] == curr:
                            break
                        
                    for j in range(true_i_order[find_index]):
                        row.append(row[0].copy())
                        row[i+j+1][2] = true_order[0]
                        if stop == true_order[0]:
                            break
                        true_order.pop(0)
                        # for {stop} values equal to given build_event
                        if stop == row[0][2]:
                            return row[0]
            # add manufacturing build_events
            if 'M' in item:
                m_build_event.append(item)

        if m_build_event:
            for k in row:
                if not m_build_event:
                    break
                m_item = m_build_event.pop(0)
                clean_me = re.sub('[{}]', '', m_item)
                clean_me = clean_me.strip()
                clean_me = clean_me.split('-')
                new_clean = clean_me[1]

                if k[2] is not np.nan:
                    if new_clean[len(new_clean)-2 :] in k[2]:
                        k[5] = clean_me[0] + '-' + clean_me[1]
                else:
                    k[5] = new_clean
                if 'VB' in new_clean:
                    k[5] = new_clean
                    
        if date_effectivity:
            row[i][3] = date_effectivity[0]
            date_effectivity.pop(0)
            row[i][4] = date_effectivity[0]
            date_effectivity.pop(0)

    return row

def applicability_clean(ca_number_str, applicability_str):
    
    txt = applicability_str

    program = []
    build_event = []
    date_effectivity = []

    clean_up(txt, program, build_event, date_effectivity)

    table_list = [['ca_number', 'program', 'build_event', 'effectivity_in', 'effectivity_out', 'manufacturing_build_event', 'original_string']]
    table_list_size = len(table_list)

    row = []
    m_build_event = []

    ca_number = ca_number_str

    indx = []
    for item in program:
        indx.append(txt.find(item))

    segments = [txt[i:j] for i,j in zip(indx, indx[1:]+[None])]
    if '\n' in txt:
        segments = txt.split('\n')

    final_rows = []

    if not program:
        xx_row = []
        program.append('no program given')
        xx_entry = add_new_row(xx_row, ca_number, program, build_event, date_effectivity, applicability_str)

        if type(xx_entry[0]) is str:
                xx_row = []
                xx_row.append(xx_entry)
        for entry in xx_row:
            final_rows.append(entry)
    else:
        for index, item in enumerate(segments):
            xx_row = []
            program_1 = []
            build_event_1 = []
            date_effectivity_1 = []
            new_stop = ''

            clean_up(item, program_1, build_event_1, date_effectivity_1)

            stop = ''
            
            for index, item_2 in enumerate(build_event_1):
                if '{' and '}' in item_2:
                    curr = build_event_1.pop(index)
                    stop = re.sub('[{}]', '', curr)
                    stop = stop.strip()
                    stop = stop.split('-')
                    if stop[1] == 'DVT':
                        stop[1] = 'DVT1'
                    new_stop = stop[1]
                    break
                else:
                    new_stop = ''

            if build_event_1:
                zz = item.index(build_event_1[0])
                if item[zz + len(build_event_1[0])] != '<':
                    stop = build_event_1[0].strip()
                    stop = stop.split('-')
                    new_stop = stop[1]

            xx_entry = add_new_row(xx_row, ca_number, program_1, build_event_1, date_effectivity_1, applicability_str, new_stop)

            if xx_entry:
                if type(xx_entry[0]) is str:
                    xx_row = []
                    xx_row.append(xx_entry)
            for entry in xx_row:
                final_rows.append(entry)
    
    df = pd.DataFrame(final_rows, columns = table_list)
    df = df.drop_duplicates()
    df = df.reset_index(drop=True)

    return df

or_list = []
global df_final
df_final = pd.DataFrame()

for index, row in df_ca_summary.iterrows():

    if pd.isnull(row['applicability']):
        row['applicability'] = ''

    if ' OR ' in row['applicability']:
        or_list.append([row['name'], row['applicability']])
    else:
        df_add = applicability_clean(row['name'], row['applicability'])
        df_final = df_final.append(df_add)

    
# df_final contains output of all reformatted applicability fields

# df_or_list contains all  'or' test cases

df_or_list = pd.DataFrame(or_list)
df_or_list